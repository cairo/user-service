FROM fabric8/java-alpine-openjdk8-jre
COPY UserRestAdapter/target/UserRestAdapter-thorntail.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
    -Djava.net.preferIPv4Addresses=true\
    -jar UserRestAdapter-thorntail.jar