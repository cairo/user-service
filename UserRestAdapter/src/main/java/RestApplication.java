import cairo.IUserController;
import cairo.IUserDB;
import cairo.UserController;
import cairo.UserDB;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
/**
 * @author Alexander
 * The basis for user service Rest adapter.
 * Generates a new IUserController interface on construction
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    public static IUserController userController;

    public RestApplication(){
        super();
        userController = new UserController(new UserDB());
    }
}
