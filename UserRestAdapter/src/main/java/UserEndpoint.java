import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import cairo.DatabaseException;
import cairo.User;
import cairo.Customer;
import cairo.Merchant;

/**
 * @author Alexander
 * Rest endpoint for communicating with user service
 */
@Path("/usermanager")
public class UserEndpoint {

	/**
	 * @author Alexander
	 * Accepts a GET request returning if a user with the specified cpr number is found
	 * @param String cpr 
	 * @param boolean isCustomer True if its a customer, false if a merchant 
	 * @return String Found or not Found
	 */
    @GET
    @Path("{usercpr}")
    @Produces("text/plain")
    public String doGet(@PathParam("usercpr") String cpr, @QueryParam("iscustomer")  boolean isCustomer){
        boolean userExists;
        if(isCustomer){
            userExists = RestApplication.userController.validateCustomer(cpr);
        }
        else{
            userExists = RestApplication.userController.validateMerchant(cpr);
        }

        if(userExists){
            return "User Found";
        }
        else{
            return "User Not Found";
        }
    }

    /**
     * @author Alexander
     * Accepts a POST request for adding a user. 
     * @param User temp_user A user with data supplied in fields
     * @param boolean isCustomer True if its a customer, false if a merchant 
     * @return Response The response containing status and result message
     */
    @POST
    @Consumes("application/json")
    @Produces("text/plain")
    public Response doPost(User temp_user, @QueryParam("iscustomer") boolean isCustomer) {
        User user;
        if(isCustomer){
            user = new Customer(temp_user);
        }
        else{
            user = new Merchant(temp_user);
        }

        try {
            RestApplication.userController.registerUser(user);
        } catch (DatabaseException e){
            System.out.println("The user already exists");
            return Response.notModified("The user already exists").build();
        }
        System.out.println("The user was added");
        return Response.ok("The user was added").build();
    }

    /**
     * @author Alexander
     * Accepts a DELETE request for deleting a user.
     * @param String usercpr
     * @return Response The response containing status and result message
     */
    @DELETE
    @Path("{usercpr}")
    @Produces("text/plain")
    public Response doDelete(@PathParam("usercpr") String usercpr){
        try {
            RestApplication.userController.deleteUser(usercpr);
        } catch (DatabaseException e){
            return Response.notModified("The user was not found").build();
        }

        return Response.ok("The user was deleted").build();
    }



}
