package cairo;
/**
 * @author Alexander
 * Defines an exception containing scenarios where validation of one or more users are unsuccessful
 */
public class ValidationFailureException extends Exception {
    public ValidationFailureException(String message) {
        super(message);
    }
}
