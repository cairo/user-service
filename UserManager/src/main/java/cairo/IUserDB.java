package cairo;

/**
 *@author Henning
 *
 * This is the interface for the User database.
 */
public interface IUserDB {
    void insertUser(User user) throws DatabaseException;

    void removeUser(String cpr) throws DatabaseException;

    boolean validateCustomer(String cpr);

    boolean validateMerchant(String cpr);

}
