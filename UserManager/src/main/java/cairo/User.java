package cairo;
import java.util.Objects;

/**
 * @author Henning
 * This class establishes the general structure of the user data objects.
 * The class is only meant to be inherited by the Customer and Merchant objects,
 * but to avoid issues with the rest interface it has not been made abstract.
 */
public class User {

    public User() {

    }

    /**
     * The is a constructor for the class, which will also be used in
     * the child classes. The account is initialized as an empty string
     * such that users cannot initialize themselves with any account number.
     *
     * @param User user
     */
    public User(User user) {
        this.cpr = user.cpr;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.account = "";
    }

    public User(String id, String firstName, String lastName) {
        this.cpr = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.account = "";
    }

    private String cpr;

    private String firstName;

    private String lastName;

    private String account;

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return user.cpr.equals(this.cpr) &&
                user.firstName.equals(this.firstName) &&
                user.lastName.equals(this.lastName) &&
                user.account.equals(this.account);
    }
}
