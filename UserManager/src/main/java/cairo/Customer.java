package cairo;

/**
 * @author Henning
 *
 * This is the customer class, where any methods and properties
 * that diverge from the merchant class can be added.
 */
public class Customer extends User{

    public Customer(User user){
        super(user);
    }

    public Customer(String cpr, String firstName, String lastName){
        super(cpr, firstName, lastName);
    }
}
