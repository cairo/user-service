package cairo;
/**
 * @author Alexander
 * Interface any class can declare if communication with a UserController is needed
 */
public interface IUserController {

	/**
	 * @author Alexander
	 * Function for checking if a customer exists in the user-service
	 * @param String cpr The CPR number of the customer
	 * @return boolean True if the customer exists.
	 */
    boolean validateCustomer(String cpr);

	/**
	 * @author Alexander
	 * Function for checking if a merchant exists in the user-service
	 * @param String cpr The CPR number of the customer
	 * @return boolean True if the merchant exists.
	 */
    boolean validateMerchant(String cpr);
    
    /**
     * @author Alexander
     * Function for validating a customer and merchant in a PayRequestCPR object.
     * Its up to declaring classes to redirect the results to requesting services. 
     * @param PayRequestCPR pr Object containing customer cpr, merchant cpr and amount in a transaction
     * @throws ValidationFailureException
     * @return nothing
     */
    void validateUsersForPayment(PayRequestCPR pr) throws ValidationFailureException;

    /**
     * @author Alexander
     * Function for registering a user
     * @param User user
     * @throws DatabaseException
     * return nothing
     */
    void registerUser(User user) throws DatabaseException;

    /**
     * @author Alexander
     * Function for deleting a user with a given cpr
     * @param String cpr
     * @throws DatabaseException
     * return nothing
     */
    void deleteUser(String cpr) throws DatabaseException;
}
