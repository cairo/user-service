package cairo;

import java.io.IOException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author Mathias Kirkeskov Madsen
 * MQ Producer
 */

public class MQProducer {
	private static String QUEUE_NAME = "UsPs";

    /**
     * @author Mathias Kirkeskov Madsen
     * For producing MQ regarding whether a user exists and is a correct class
     * @param pr
     */
    public void produceUserValidation(PayRequestCPR pr) {
        System.out.println("producer reached");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        try {
        	Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            
            byte[] data = SerializationUtils.serialize(pr);
            System.out.println("User service producer validated users and produces data");
            System.out.println("Data: CustomerId: " + pr.customerId + ", MerchId: " + pr.merchantId + ", Amount: " + pr.amount);

            channel.basicPublish("",QUEUE_NAME,null, data );
            System.out.println("DATA SENT");
            channel.close();
            connection.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
}
