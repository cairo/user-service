package cairo;

import java.io.IOException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * message queue consumer for communication with the PsUs queue.
 * @author Michael
 */

public class MQConsumer {
	private static String QUEUE_NAME = "PsUs";
    private IUserController userController;

    public MQConsumer(IUserController IUC) throws IOException {
        System.out.println("CONSUME, PsTm");
        this.userController = IUC;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("queue declared");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                System.out.println("\nCalling handleDelivery from User Service consumer");
                PayRequestCPR pr = SerializationUtils.deserialize(body);
                System.out.println("Data: CustomerId: " + pr.customerId + ", MerchId: " + pr.merchantId + ", Amount: " + pr.amount);
                try {                		            	
                	userController.validateUsersForPayment(pr);
            	} catch (ValidationFailureException e) {
            		System.out.println("Validation Failure thrown with message: " + e.getMessage());
            	}
            }
        };

        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
