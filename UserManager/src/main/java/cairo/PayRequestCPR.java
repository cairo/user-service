package cairo;

import java.math.BigDecimal;

/**
 * @author Jonas
 * Used to pass between message queues. This is recieved from payment-service and return to
 * payment service if both the merchant and the customer is valid in the system
 */

public class PayRequestCPR implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
    public PayRequestCPR(){

    }

    public String merchantId;

    public String customerId;

    public BigDecimal amount;
}
