package cairo;
/**
 * @author Alexander
 * Implements IUserController
 * Main logic for coupling Rest application requests to the actual database functionality
 */
public class UserController implements IUserController {
    IUserDB db;
    MQConsumer mqc;
    MQProducer mqp;

    public UserController(IUserDB temp_db) {
        db = temp_db;
        mqp = new MQProducer();
        try {        	
        	mqc = new MQConsumer(this);
        } catch (Exception e) {
        	System.out.println("Error from MQConsumer in UserController: " + e.getMessage());
        	e.printStackTrace();
        }
    }

    /**
     * @author Alexander
     * Function for validating if a customer with a specified cpr number exists in a database
     * @param String cpr
     * @return boolean True if customer exists
     */
    public boolean validateCustomer(String cpr) {
        return db.validateCustomer(cpr);
    }

    /**
     * @author Alexander
     * Function for validating if a merchant with a specified cpr number exists in a database
     * @param String cpr
     * @return boolean True if merchant exists
     */
    public boolean validateMerchant(String cpr) {
    	return db.validateMerchant(cpr);
    }
    
    /**
     * @author Alexander
     * Function for validating a customer and a merchant in a PayRequestCPR, used for a transaction
     * @param PayRequestCPR pr Object containing customer cpr, merchant cpr and amount to be transfered
     * @return nothing
     */
    public void validateUsersForPayment(PayRequestCPR pr) throws ValidationFailureException {
    	boolean customerSuccess = validateCustomer(pr.customerId);
    	boolean merchantSuccess = validateMerchant(pr.merchantId);
    	
        if (customerSuccess && merchantSuccess) {
        	mqp.produceUserValidation(pr);
        }
        else {
        	throw new ValidationFailureException("Validation failed for: " + (customerSuccess ? "" : "customer,")
        			+ (merchantSuccess ? "" : "merchant"));
        }
    }
    
    /**
     * @author Alexander
     * Function for registering a user in the database
     * @param User user
     * @return nothing
     */
    public void registerUser(User user) throws DatabaseException {
        db.insertUser(user);
    }
    
    /**
     * @author Alexander
     * Function for deleting a user from the database
     * @param String cpr
     * return nothing
     */
    public void deleteUser(String cpr) throws DatabaseException{
        db.removeUser(cpr);
    }
}
