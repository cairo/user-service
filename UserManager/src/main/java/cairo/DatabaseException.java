package cairo;

/**
 * @author Henning
 *
 * This is a database exception, which is thrown when there are
 * issues with the database. Currently, it is only thrown when
 * attempting to add a user already in the database, or when trying
 * to delete a user that does not exist.
 */
public class DatabaseException extends Exception{
    String message;

    public DatabaseException(String message){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
