package cairo;

/**
 * @author Henning
 *
 * This is the merchant class, where any methods and properties
 * that diverge from the customer class can be added.
 */
public class Merchant extends User{
    public Merchant(User user){
        super(user);
    }

    public Merchant(String cpr, String firstName, String lastName){
        super(cpr, firstName, lastName);
    }
}

