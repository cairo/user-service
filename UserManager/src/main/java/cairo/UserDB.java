package cairo;
import java.util.ArrayList;

/**
 * @author Henning
 *
 * This class implements the User database, along with the methods
 * that allow other classes to access it.
 * The methods that other classes are allowed to access
 * are declared in the IUserDB interface.
 */
public class UserDB implements IUserDB{

    private ArrayList<User> userList;

    public UserDB(){
        userList = new ArrayList<User>();
    }

    public void insertUser(User user) throws DatabaseException {
       for(User temp_user: userList){
           if(temp_user.getCpr().equals(user.getCpr())){
               throw new DatabaseException("The user already exists");
           }
       }

       userList.add(user);
    }

    public void removeUser(String cpr) throws DatabaseException {
        boolean userFound = false;
        for(User temp_user: userList){
            if(temp_user.getCpr().equals(cpr)){
                userFound = true;
                userList.remove(temp_user);
                break;
            }
        }
        if(!userFound){
            throw new DatabaseException("The given user does not exist");
        }
    }

    /**
     * This method checks whether a given user exists in the database.
     *
     * @param String cpr
     * @return User temp_user
     */
    private User validateUser(String cpr){
        for(User temp_user: userList){
            if(temp_user.getCpr().equals(cpr)){
                return temp_user;
            }
        }

        return null;
    }

    /**
     * This function checks whether a given user exists in the database
     * and if they are of the type Customer.
     * @param String cpr
     * @return boolean
     */
    public boolean validateCustomer(String cpr) {
        User user = validateUser(cpr);
        if(user == null){
            return false;
        }
        else {
            return user instanceof Customer;
        }
    }

    /**
     * This function checks whether a given user exists in the database
     * and if they are of the type Merchant.
     *
     * @param String cpr
     * @return boolean
     */
    public boolean validateMerchant(String cpr){
        User user = validateUser(cpr);
        if(user == null){
            return false;
        }
        else {
            return user instanceof Merchant;
        }
    }
}
