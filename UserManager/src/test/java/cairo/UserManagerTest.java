package cairo;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;

/**
 * @author Henning
 *
 * These are the unit tests for the user-service micro service.
 * They test the business logic of the user controller and the user database.
 */
public class UserManagerTest {

    @Test
    public void testValidateTrue() throws DatabaseException{
        IUserDB db = new UserDB();
        IUserController controller = new UserController(db);
        User customer = new Customer("1234567892", "Henning", "Frederiksen");
        User merchant = new Merchant("1234567890", "Henning", "Frederiksen");
        db.insertUser(customer);
        db.insertUser(merchant);
        assertTrue(controller.validateCustomer(customer.getCpr()));
        assertTrue(controller.validateMerchant(merchant.getCpr()));
    }

    @Test
    public void testValidateNonexistentUser(){
        IUserDB db = new UserDB();
        IUserController controller = new UserController(db);
        User customer = new Customer("1234567892", "Henning", "Frederiksen");
        User merchant = new Merchant("1234567890", "Henning", "Frederiksen");
        assertFalse(controller.validateCustomer(customer.getCpr()));
        assertFalse(controller.validateMerchant(merchant.getCpr()));
    }

    @Test
    public void testDeleteCostumer() throws DatabaseException{
        User user = new Customer("1234567890", "Henning", "Frederiksen");
        IUserDB db = new UserDB();
        IUserController controller= new UserController(db);
        db.insertUser(user);
        assertTrue(db.validateCustomer(user.getCpr()));
        controller.deleteUser(user.getCpr());
        assertFalse(db.validateCustomer(user.getCpr()));

    }
    @Test
    public void testCreateCustomer() throws DatabaseException{
        User user = new Customer("1234567890", "Henning", "Frederiksen");
        IUserDB db= new UserDB();
        IUserController controller = new UserController(db);
        controller.registerUser(user);
        assertTrue(db.validateCustomer(user.getCpr()));
    }

    @Test
    public void testCreateMerchant() throws DatabaseException {
        User user = new Merchant("1234567890", "Henning", "Frederiksen");
        IUserDB db = new UserDB();
        IUserController controller = new UserController(db);
        controller.registerUser(user);
        assertTrue(db.validateMerchant(user.getCpr()));
    }

    @Test()
    public void testDeleteException(){
        IUserDB db = new UserDB();
        IUserController controller = new UserController(db);
        User merchant = new Merchant("1234567890", "Henning", "Frederiksen");
        try {
            controller.deleteUser(merchant.getCpr());
        } catch (DatabaseException e){
            assertTrue("The given user does not exist".equals(e.getMessage()));
        }
    }

    @Test()
    public void testAddDuplicateException(){
        IUserDB db = new UserDB();
        IUserController controller = new UserController(db);
        User merchant = new Merchant("1234567890", "Henning", "Frederiksen");
        try {
            controller.registerUser(merchant);
            controller.registerUser(merchant);
        } catch (DatabaseException e){
            assertTrue("The user already exists".equals(e.getMessage()));
        }
    }

}
